<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('user/index');
});

// Authentication Routes...
$this->get('login', 'Auth\AuthController@showLoginForm');
$this->post('login', 'Auth\AuthController@login');
$this->get('logout', 'Auth\AuthController@logout');


// Password Reset Routes...
$this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
$this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
$this->post('password/reset', 'Auth\PasswordController@reset');

Route::get('/home', 'HomeController@index');

Route::group([
	'middleware' => 'auth',
	'namespace' => 'Admin',
	'prefix' => 'admin',
	], function () {

		Route::get('/', function () {
        	return view('admin/index');
    	});

    	Route::resource('categories', 'CategoryController');
    	Route::resource('news', 'NewsController');
    	Route::get('news/{status?}/{category?}', 'NewsController@index');

    
});
