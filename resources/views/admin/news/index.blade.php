@extends('layouts.admin')
@push('meta')
<meta name="csrf-token" content="<?= csrf_token() ?>" />
<meta name="csrf-param" content="_token" />
@endpush
@section('title', 'სიახლეები')

@section('maintitle', 'სიახლეები')
@section('content')

 <div class="col-md-2 pull-right">
        {!!
            Form::select(
                'status',
                 [
                    'active'=>'აქტიური',
                    'pasive'=>'პასიური'
                ],
                $status,
                ['class' => 'form-control','id'=>'status']
            )
        !!}
</div>
 <div class="col-md-2 pull-right">
        {!!
            Form::select(
                'category',
                $categories,
                '',
                ['class' => 'form-control','id'=>'category']
            )
        !!}
</div>

<table class="table table-bordered table-striped table-hover" style="margin-top: 40px">
    <thead>
      <tr>
        <th>სათაური</th>
        <th>სტატუსი</th>
        <th>კატეგორია</th>
        <th>სურათი</th>
        <th></th>
        <th></th>

      </tr>
    </thead>
    <tbody>
    @foreach($news as $item)
      <tr>
        <td>{{$item->name}}</td>
        <td>{{$item->getStatus()}}</td>
        <td>
            @if(isset($item->category))
                {{$item->category->name}}
            @endif
        </td>
        <td><img src="{{$item->image}}" style="height: 60px;"></td>
        <td>
        	<a href="{{url('admin/news/'.$item->id.'/edit')}}"><span class="fa fa-pencil"></span></a>
        </td>
        <td>
        	<a href="{{ route('news.destroy',array($item->id)) }}" data-method="delete" rel="nofollow" data-confirm="Are you sure you want to delete this?"><span class="fa fa-remove"></span></a>
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>

@endsection
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ujs/1.2.2/rails.min.js"></script>
<script type="text/javascript">
    url='{{url('')}}'
    $( "#status" ).on('change',function() {

        my_url=url+"/admin/news/"+$(this).val();
        console.log(my_url);
        location.href=my_url;
    })
    $( "#category" ).on('change',function() {
        console.log($(this).val());
        my_url=url+"/admin/news/"+$(this).val();

        location.href=my_url;
    })
</script>
@endpush

