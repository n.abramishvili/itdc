@extends('layouts.admin')

@section('title', 'კატეგორიები')
@section('maintitle', 'კატეგორიის შეცვლა')
@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::open([

		'url' => '/admin/categories/'.$category->id,
		'method'=>'PUT',
		'class' => 'form-horizontal'

	]) !!}

      <div class="form-group">
          <label class="select2_multiple col-md-4 col-xs-12 control-label" >მშობელი</label>
          <div class="col-md-4 col-xs-12">   
        {!!
          Form::select(
            'parent',
            $categories,
            old('parent',$category->parent_id),
            ['class'=>'form-control']
            
          )
        !!}    
        </div>
      </div>

      <div class="form-group">
         <label class="control-label col-md-4 col-xs-12">სახელი</label>
          <div class="col-md-4 col-xs-12">
              <input  type="text" class="form-control" value="{{ old('name',$category->name) }}" name='name' placeholder="სახელი"
                data-validation="required" 
		 		data-validation-length="3-12" 
		 		data-validation-error-msg="სახელი აუცილებელია">
          </div>
      </div>

  		<div class="form-group">
         <label class="control-label col-md-4 col-xs-12">აღწერა</label>
          <div class="col-md-4 col-xs-12">
              <input type="text" class="form-control" value="{{ old('description',$category->description) }}" name='description' placeholder="აღწერა"
               data-validation="required" 
		 	   data-validation-length="3-12" 
		       data-validation-error-msg="აღწერა აუცილებელია">
          </div>
      </div>
      
      <div class="form-group">
			  <label class="col-md-4 col-xs-12 control-label" for="singlebutton"></label>
			  <div class="col-md-4 col-xs-12">
			    <button type="submit" class="btn btn-success">შეცვლა</button>
			  </div>
	</div>
  {!! Form::close() !!}

@endsection
@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>

  $.validate();


</script>
@endpush
