<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Models\News;

class Category extends Model
{
	public function childs()
    {
        return $this->hasMany('App\Models\Category','parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Category','parent_id');
    }

    public function news()
    {
        return $this->hasMany('App\Models\News');
    }

    public static  function selectArray($value=0,$id=0)
    {
        $catArray=[];
        if ($value==0) {
           $catArray[0]='';
        }
        
        $categories=Category::all();
        foreach ($categories as  $category) {
            if ($category->id!=$id) {
                $catArray[$category->id]=$category->name;
            }
        	
        }

        return $catArray;
    }

    public static  function buildMenu($parent=0)
    {  
        $result='';
        $categories=Category::all();
        
        foreach ($categories as $row){
            if ($row->parent_id == $parent){
                $url=url('/category/'.$row->id);
                $result.= '<li><a href="'.$url.'">'.$row->name .'</a>';
                if (count($row->childs)>0){
                    $result.= '<ul class="dropdown-menu" >';
                    $result.= Category::buildMenu($row->id);
                    $result.= '</ul>';
            }
            $result.= '</li>';
          }

      }

      

      return $result;

  }
    
}